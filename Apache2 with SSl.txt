////// HOW TO HOSTING YOUR WEB ON APACHE2 /////////////
1.STEP 
    -Login as root user

2.STEP 
    -Update your ubuntu 
        sudo apt-get update 
    
    -Install Apache2
        sudo apt-get install apache2
3.STEP 
    -Create your hosting directory
        sudo mkdir -p /var/www/btnhd.com/public_html
    -To grand permission access to that directory
        sudo chown -R $USER:$USER /var/www/btnhd.com/public_html
    -To Set permission for the main root folder www
        sudo chown -R 755 /var/www
4.STEP 
    -Create your test host html page
        sudo nano /var/www/btnhd.com/public_html/index.html
    -Add some html code to index.html file
        <html>
            <head>
                <title>btnhd-test</title>
            </head>

            <body>
                <h2>Welocome to btnhd.com</h2>
            </body>
        </html>
    -Create your virtual host file
        sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/btnhd.com.conf
    -Config your btnhd.com.conf file 
        sudo nano /etc/apache2/sites-available/btnhd.com.conf

        <VirtualHost *:80>
                ServerAdmin info@btnhd.com
                ServerName btnhd.com
                ServerAlias www.btnhd.com
                DocumentRoot /var/www/btnhd.com/public_html
                ErrorLog ${APACHE_LOG_DIR}/error.Log
                CustomLog ${APACHE_LOG_DIR}/access.Log combined
        </VirtualHost>
5.STEP
    -Enable your VirtualHost file
        cd /etc/apache2/sites-available
        sudo a2ensite btnhd.com.conf

    -Disable your default host file
        cd /etc/apache2/sites-available
        sudo a2dissite 000-default.com.conf
    
    -Restarting your APACHE2 Server
        sudo service apache2 restart 

6.STEP 
    -Setting up your localhost to point to our side 
        sudo nano /etc/hosts

        add your server ip as your localhost ip address of your site 
            example 192.168.47.201  btnhd.com



/////////////// Configure HTTPS //////////////////

1.STEP

    -You can enable the SSL module by running
        sudo a2enmod ssl
    -After you have enabled SSL, you’ll have to restart the Apache service for the change to be recognized
        sudo service apache2 restart
    -Generate a Self-signed Certificate
        sudo openssl genrsa -out ca.key 2048
    -Then generate a certificate signing request (ca.csr) using the following command:
        sudo openssl req -nodes -new -key ca.key -out ca.csr

        After that you will need to fill out some informantion of your site location
        example: my site locat at combodia sihanouk ville

                Country Name (2 Letter code) [AU]:KH
                State or Province Name (full name) [Some-State]:SIHANOUKVILLE
                Locality Name (eg, city) []:SIHANOUKVILLE
                Organization Name (eg, company) [Internet Widgits Pty Ltd]:Tech Emergente
                Organization Unit Name (eg, section) []:Information Technology
                Common Name (e.g. server FQDN or YOUR name) []:btnhd.com
                Email Address []:sophonaratouch@gmail.com
    -Lastly, generate a self-signed certificate (ca.crt) of X509 type valid for 365 keys
        sudo openssl x509 -req -days 365 -in ca.csr -signkey ca.key -out ca.crt

    -Create a directory to place the certificate files we have created
        sudo mkdir /etc/apache2/ssl
    -Next, copy all certificate files to the “/etc/apache2/ssl” directory
        sudo cp ca.crt ca.key ca.csr /etc/apache2/ssl/

2.STEP
        -Configure Apache to Use the SSL Certificate:
            Now all the certificates are ready. The next thing to do is to set up the Apache to display the new certificate.
            For this, you need to enable SSL support on the Apache default virtual host file located in the /etc/apache2/sites-enable/ directory.
    
            You can do this by editing the Apache default virtual host config file.
                sudo nano /etc/apache2/sites-enable/btnhd.com.conf

                Comment out all the lines by adding a “#” in front of each line and add the following lines:
                <VirtualHost *:80>
                	ServerAdmin info@btnhd.com
                	ServerName btnhd.com
                	ServerAlias www.btnhd.com
                	DocumentRoot /var/www/btnhd.com/public_html
                
                	ErrorLog ${APACHE_LOG_DIR}/error.log
                	CustomLog ${APACHE_LOG_DIR}/access.log combined
                
                </VirtualHost>
                
                <VirtualHost _default_:443>
                    ServerName info@btnhd.com
                    ServerAlias btnhd.com
                    ServerAdmin www.btnhd.com
                    SSLEngine on
                    SSLCertificateFile /etc/apache2/ssl/ca.crt
                    SSLCertificateKeyFile /etc/apache2/ssl/ca.key
                    SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown
                    CustomLog ${APACHE_LOG_DIR}/ssl_request_log "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"
                    DocumentRoot /var/www/btnhd.com/public_html
                    <directory /var/www/btnhd.com/public_html>
                        Options All
                                AllowOverride All
                                Require all granted
                    </directory>
                    ErrorLog ${APACHE_LOG_DIR}/ssl-example.com-error.log
                    CustomLog ${APACHE_LOG_DIR}/ssl-example.com-access.log combined
                </VirtualHost>


                # vim: syntax=apache ts=4 sw=4 sts=4 sr noet

            Save and close the file, then restart Apache.
                sudo /etc/init.d/apache2 restart

Final Step Test HTTPS URL
        To verify the Apache (HTTPS) web server, open your web browser and type your server IP Address (with “https://btnhd.com,” for example: “https://192.168.47.201”).



/////////////// installing git on ubuntu //////////
https://www.liquidweb.com/kb/install-git-ubuntu-16-04-lts/


///////////////////// for hosting PHP //////////////
https://websiteforstudents.com/setup-apahce2-with-php-support-on-ubuntu-servers/


/etc/apache2/mods-enabled/dir.conf:

/////////////////// config mysql database /////////////

https://hostadvice.com/how-to/how-to-install-apache-mysql-php-on-an-ubuntu-18-04-vps/

///// import database mysql /////////////
mysql -u <username> -p<PlainPassword> <databasename> < <filename.sql>
mysql -u root -p wp_users < wp_users.sql

//// list database ////////////
mysql -u root -p
mysql> show databases;
mysql> create database <databasename>;
/// show table ///
$ mysql -u root -p
mysql> use pizza_store;

/////////////////////// ubuntu redirect http to https //////////
https://www.tecmint.com/redirect-http-to-https-on-apache/


//////////////// change timezone to cambododia /////////////
sudo nano /etc/php/7.x/apache2/php.ini

date.timezone = "Asia/Bangkok" 

